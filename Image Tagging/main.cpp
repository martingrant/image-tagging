//
//  Multimedia Systems and Applications 4
//  2015/2016 Coursework
//  Building a Photo Tag recommendation system on a Flickr collection
//
//  Martin Grant
//  2216713G
//
//  Created by Martin Grant on 23/10/2015.
//  Copyright © 2015 Martin Grant. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <cmath>


// Function to load tags from a file into a vector
void loadTags(std::string filePath, std::vector<std::pair<std::string, int>>& tagsVector)
{
    // Open the file
    std::ifstream tags(filePath);
    
    // String to hold current line
    std::string currentLine = "";
    
    // String to hold tag in current line
    std::string tag = "";
    
    // Int to hold tag occurence in current line
    int tagOccurence = 0;

    // Read till end of the file
    while (tags.good())
    {
        // Get the current line
        getline(tags, currentLine);
        
        // Get the tag on the current line
        tag = currentLine.substr(0, currentLine.find(","));
        
        // Get the tag occurence on the current line
        // currentLine.find(",") + 1 to get the first character after the ","
        // val.pop_back() to get rid of the "\r" character
        std::string val = currentLine.substr(currentLine.find(",") + 1, currentLine.size());
        val.pop_back();
        
        tagOccurence = std::stoi(val);
        
        // Make a pair using the current tag and it's occurence value and push it onto the vector
        tagsVector.push_back(std::make_pair(tag, tagOccurence));
    }
}


// Function to load photo data into a vector
void loadPhotos(std::string filePath, std::vector<std::vector<std::string>>& photosVector)
{
    // Open the file
    std::ifstream photos(filePath);
    
    // IDs for last and current image that has/is being checked
    int lastImage = 0;
    int currentImage = 0;
    
    // String to hold current line
    std::string value = "";
    
    while (photos.good())
    {
        // Get current line
        getline(photos, value);
        
        // Get current image ID
        currentImage = stoi(value.substr(0, value.find(',')));
        
        // Get image tag
        std::string tag = value.substr(value.find(',') + 1, value.size());
        
        // Remove "\r" character
        tag.pop_back();
        
        // If current tag is from previous image, add it to that image
        if (lastImage == currentImage)
        {
            photosVector.back().push_back(tag);
        }
        else // If we have moved onto a new image, create a new vector, push the tag onto it, and push the new vector onto the main vector
        {
            std::vector<std::string> newPhoto;
            newPhoto.push_back(tag);
            
            photosVector.push_back(newPhoto);
        }
        
        // Set the last image to the one which was just checked
        lastImage = currentImage;
    }
}


// Function to generate the tag co-occurence matrix using the photos and tag vectors
void generateMatrix(std::vector<std::vector<std::string>>& photos, std::vector<std::pair<std::string, int>>& tags, int matrix[100][100])
{
    // Loop for whole tags vector
    for (int i = 0; i < tags.size(); ++i)
    {
        // Get the tag at the current position in the vector
        std::string currentTag = tags[i].first;
        
        // Loop for all other tags in the vector
        for (int j = 0; j < tags.size(); ++j)
        {
            // Get the tag at the current inner loop position
            std::string checkTag = tags[j].first;
            
            // Integer variable that will be increment on occurences of the current and check tags
            int score = 0;
            
            // If we are checking a tag against itself, set the score to -1
            if (currentTag == checkTag)
            {
                score = -1;
            }
            else // Else check the photos vector to see if both of these tags exist together in any images
            {
                // Loop for each photo in the vector
                for (int i = 0; i < photos.size(); ++i)
                {
                    // An innerscore variable to check if both tags exist in the same image
                    int innerscore = 0;
                    // Loop for each tag in the current photo, increment innerscore if the tags are found
                    for (int j = 0; j < photos[i].size(); ++j)
                    {
                        if (currentTag == photos[i][j])
                            innerscore++;
                        
                        if (checkTag == photos[i][j])
                            innerscore++;
                    }
                    // If innerscore is greater than 1, it means both currentTag and checkTag are in this photo, increment the score
                    if (innerscore > 1)
                        score++;
                }
            }
            // Set the current matrix position to the score variable
            matrix[i][j] = score;
        }
    }
}


// Function to generate a tag co-occurence matrix with IDF values
void generateIDFMatrix(std::vector<std::pair<std::string, int>>& tags, float numberOfImages, int matrix[100][100], float IDFMatrix[100][100])
{
    // Loop through the tag vector
    for (int i = 0; i < tags.size(); ++i)
    {
        for (int j = 0; j < tags.size(); ++j)
        {
            // If we aren't checking a tag against itself, calculate the IDF value
            if (j != i)
            {
                IDFMatrix[i][j] = std::log(numberOfImages/ tags[j].second) * matrix[i][j];
            }
            else // Set the current position to -1
            {
                IDFMatrix[i][j] = -1;
            }
        }
    }
}


// Template function to export a matrix to a .csv file. Template is used so different matrix types can be passed (e.g. an int matrix or a float matrix)
template <typename T>
void exportMatrix(std::string fileName, std::vector<std::pair<std::string, int>>& tags, T const & matrix)
{
    // String to hold file contents, with "," so the first cell in the file is empty
    std::string output = ",";
    
    // Add tags names for the first row
    for (int i = 0; i < tags.size(); ++i)
    {
        output += tags[i].first + ",";
    }
    
    // Add return carriage character to move to second row
    output += "\r";
    
    // Add all other rows to the string, looping for the whole matrix
    for (int i = 0; i < tags.size(); ++i)
    {
        for (int j = 0; j < tags.size(); ++j)
        {
            // If we are on the first column of this iteration, add the tag name
            if (j == 0)
            {
                output += tags[i].first + "," + std::to_string(matrix[i][j]) + ",";
            }
            else // Else, add the tag value from the matrix at the current position
            {
                output += std::to_string(matrix[i][j]) + ",";
            }
        }
        // Add return carriage to move to new line, at the end of each row
        output += "\r";
    }
    
    // Create new .csv file to hold the tag matrix
    std::ofstream file(fileName, std::ofstream::binary);
    
    // Write the file contents string to the file then close the file
    file.write(output.c_str(), output.size());
    file.close();
}


// Function to calculate the top 5 occuring tags for a target tag
void calculateTop5Tags(std::string targetTag, std::vector<std::pair<std::string, int>>& tags, int matrix[100][100], float IDFMatrix[100][100])
{
    // Loop for the whole tag vector
    for (int i = 0; i < tags.size(); ++i)
    {
        // If the target tag has been found in the vector
        if (tags[i].first == targetTag)
        {
            // Create a int and float vector for normal values and IDF values to hold a full row
            std::vector<std::pair<int, std::string>> rowVectorInt;
            std::vector<std::pair<float, std::string>> rowVectorFloat;
            
            // Add the tags with their occurence values along the target tag's row
            for (int j = 0; j < tags.size(); ++j)
            {
                rowVectorInt.push_back(std::make_pair(matrix[i][j], tags[j].first));
                rowVectorFloat.push_back(std::make_pair(IDFMatrix[i][j], tags[j].first));
            }
            
            // Sort both vectors and reverse them so the order is large -> small
            std::sort(rowVectorInt.begin(), rowVectorInt.end());
            std::sort(rowVectorFloat.begin(), rowVectorFloat.end());
            std::reverse(rowVectorInt.begin(), rowVectorInt.end());
            std::reverse(rowVectorFloat.begin(), rowVectorFloat.end());
            
            // Print out the top 5 with normal occurence values
            std::cout << "\nThe top 5 co-occuring tags for '" << targetTag << "' are:\n";
            for (int k = 0; k < 5; ++k)
            {
                std::cout << rowVectorInt[k].second << "(" << rowVectorInt[k].first << ")\t\t";
            }
            
            // Print out the top 5 with IDF values
            std::cout << "\nThe top 5 co-occuring tags (using IDF) for '" << targetTag << "' are:\n";
            for (int k = 0; k < 5; ++k)
            {
                std::cout << rowVectorFloat[k].second << "(" << rowVectorFloat[k].first << ")\t\t";
            }
            
            // Break out the loop since we are now done
            break;
        }
    }
}


// Main function. Makes calls to other functions to load required data to generate the co-occurence matrices.
int main(int argc, const char * argv[])
{
    // Load tags into a vector of strings
    std::vector<std::pair<std::string, int>> tagsVector;
    loadTags("tags.csv", tagsVector);
 
    // Load photos into a vector, of vector of strings (e.g. photosVector[0][3] = the 3rd tag in photo 0
    std::vector<std::vector<std::string>> photosVector;
    loadPhotos("photos_tags.csv", photosVector);
    
    // 2D array to hold co-occurence matrix
    int matrix[100][100];
    
    // Generate the co-occurence matrix, using the photos vector and tags vector and store the matrix in the 2d array "matrix"
    generateMatrix(photosVector, tagsVector, matrix);
    
    // 2D array to hold co-occurence matrix with IDF values
    float IDFMatrix[100][100];
    
    // Generate the co-occurence matrix for IDF values
    generateIDFMatrix(tagsVector, (float)photosVector.size(), matrix, IDFMatrix);
    
    // Save the matrices as .csv files
    exportMatrix("tagsOccurenceMatrix.csv", tagsVector, matrix);
    exportMatrix("tagsOccurenceIDFmatrix.csv", tagsVector, IDFMatrix);
    
    // Bool to operate loop for user to get top 5 tag occurences
    bool running = true;
    std::string userInput = "";
    
    // Loop asking the user for a tag to get the top 5 occurences and display the results for them
    while (running == true)
    {
        std::cout << "\n\nEnter 'exit' to stop and exit the program.";
        std::cout << "\nEnter a tag to calculate its top 5 co-occuring tags: ";
        std::cin >> userInput;
        
        // Break out of the loop if user inputs "exit"
        if (userInput == "exit")
        {
            running = false;
        }

        // Calculate the top 5 occurences for the user's input tag
        calculateTop5Tags(userInput, tagsVector, matrix, IDFMatrix);
    }
    
    return 0;
}
